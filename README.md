# README #

Scheduling library for efficient scheduling of time intervals and
merging of schedules.

This code was used in the following papers.

Fitzgerald, E., & Pióro, M. (2016, June). Performance evaluation of an intention sharing MAC scheme in wireless LANs with hidden nodes. In 2016 IEEE 17th International Symposium on A World of Wireless, Mobile and Multimedia Networks (WoWMoM) (pp. 1-10). IEEE.
https://ieeexplore.ieee.org/abstract/document/7523501

Fitzgerald, E., Bastani, S., & Landfeldt, B. (2015, November). Intention sharing for medium access control in wireless lans. In Proceedings of the 13th ACM International Symposium on Mobility Management and Wireless Access (pp. 21-30). ACM.
https://dl.acm.org/citation.cfm?id=2810367

## Configuration and Installation ##

Run "make" to compile the code including the tests. This will create an executable called "schedule", which will run the tests.

Run "make library" to compile the code as a library.

### Dependencies ###
boost (>=1.54.0), g++ (>=4.8.2), ar (if you want to make a static library, >=2.24)

## Classes and Files ##

Schedule (schedule.cc, schedule.h): The main class, representing a schedule. A schedule consists of a list of time intervals, where an interval is a pair of endpoints: a start endpoint and an end endpoint. A schedule also has a GapSet, representing the time periods between the schedule intervals.

Endpoint (endpoint.cc, endpoint.h): An endpoint of a schedule interval.

Gap (gap.cc, gap.h): A gap between two schedule intervals.

test.cc, test.h: Test functions.

## Using the Library ##

### Creating a schedule ###

First, you will need a schedule object to hold and manage your schedule.


```
#!C++

Schedule mySchedule;
```

Now you can add some time intervals to your schedule. Times are represented by uint64_t.

```
#!c++

mySchedule.addInterval(5, 15);
mySchedule.addInterval(10, 16);
mySchedule.addInterval(20, 23);
```

Your schedule would now look like this:
<5, 16>, <20, 23>
That is, it will contain two intervals, one starting at time 5 and ending at time 16, and the other starting at time 20, and ending at time 23. Note that overlapping or touching intervals are automaticzally merged as they are added to the schedule. The intervals <5, 15> and <10, 16> were merged to create a single interval <5, 16>.

Adding multiple intervals to the schedule can be expensive, since the schedule is internally represented as a linked list. A more efficient way to add multiple intervals at once is to use the Schedule::addIntervals() method, which takes a vector of intervals and adds them all to the schedule. The intervals must be in order of start time and non-overlapping.

To create a vector of five intervals and add them in a schedule, you could do the following.

```
#!c++
Schedule mySchedule;
std::vector<Interval> *intervals = new std::vector<Interval>;
for(int i = 0; i < 5; i++)
{
    start = 2*i;
    end = 2*i + 1;
    Interval interval = {new Endpoint(start, Endpoint::START), 
                           new Endpoint(end, Endpoint::END)};
    intervals->push_back(interval);
}
mySchedule.addIntervals(intervals);
```
The schedule would then look like this:
<0, 1>, <2, 3>, <4, 5>, <6, 7>, <8, 9>

If you have a lot of intervals to add but they overlap, adding them using addInterval() in reverse order of start time will be the most efficient way, as the addInterval() method will always start looking from the beginning of the schedule for the correct place to put a new interval.

### Merging Schedules ###

To merge two schedules, you can use the addSchedule() method.

```
#!c++

Schedule schedule1;
Schedule schedule2;

// ... (add intervals to the schedules)

schedule1.addSchedule(schedule2);
```
This will efficiently merge schedule2 into schedule1, without modifying schedule2. Any overlapping or touching intervals are merged together into single intervals.

### Scheduling an Interval ###

Let's say you have created your schedule, and now want to find a place in it for a new event with a given duration. To do this requires two steps. First, you need to build the gap set for your schedule and use it to find a suitable gap. Then you can insert your new event into the gap.

The gap set represents all the empty time periods between the intervals in your schedule. The gap set is defined as a std::set, which means you can use all the usual std::set methods, etc.

```
#!c++
typedef std::set<Gap *, gapCompare> GapSet;

```
The gap set is sorted first by gap duration, longest first, then by gap start time, earliest first.

To build the gap set, you use the buildGapSet() method.

```
#!c++

Schedule::GapSet gs = mySchedule.buildGapSet();
```
Now you can find a suitable gap in which to place your new interval. Let's say your new event has a duration of 8.

```
#!c++

for(Schedule::GapSet::iterator it = gs.begin(); it != gs.end(); it++)
{
    Gap *gap = *it;
    if(gap->getDuration() >= 8)
        mySchedule.insertIntoGap(gap->getStart(), 
                                  gap->getStart() + 5, gap);
        break;
}

```
This would place the new interval at the beginning of the first gap found that is big enough to hold it. You could also place your interval at the end of the gap, in the middle, or anywhere you like.

You can also use std::set::lower_bound to find the earliest gap that is big enough for your new interval. To do this, you need to make a dummy Gap of the right length as a comparison.

```
#!c++
Gap dummy(0, 8);
Schedule::GapSet::iterator gap = gs.lower_bound(&dummy);
mySchedule.insertIntoGap(gap->getStart(), 
                                  gap->getStart() + 5, *gap);

```
Note that the new gap will never be before the provided dummy, so if you want the earliest available, set the dummy gap start time to an early time (e.g. 0, or the current time). You can also exploit this to find the earliest suitable gap after a given time.

Using insertIntoGap(), you can insert as many new intervals as you like and the gap set and schedule will be updated automatically. However, if you modify the schedule in some other way (for example by calling addSchedule() or cleanUp()), you should rebuild the gap set by calling buildGapSet() again.

### Cleaning Up a Schedule ###

The cleanUp() method will remove all intervals ending before a given time. For example, the following code will remove all intervals ending before time 5.

```
#!c++

mySchedule.cleanUp(5);
```
An interval that starts before the specified time, but ends after it, would not be removed. For example if the schedule contained an interval <3, 6>, it would not be removed by the above code.

## How to cite the schedule library ##

The following BibTeX entry can be used to cite the schedule library.

```
@misc{schedule_library,
    howpublished = {\url{https://bitbucket.org/EIT_networking/schedule}},
    title = {Scheduling Library},
    author = {Emma Fitzgerald}
}
```

## Contact ##

The repository is owned and run by Emma Fitzgerald: emma.fitzgerald@eit.lth.se
