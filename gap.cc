/* Gap class for gaps: empty time between two scheduled intervals.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <boost/cstdint.hpp>
#include "endpoint.h"
#include "gap.h"

namespace schedule
{
    Gap::Gap(uint64_t start, uint64_t end, Endpoint *before, Endpoint *after)
    {
	assert(end >= start);
	this->start = uint64_t(start);
	this->end = uint64_t(end);
	this->before = before;
	this->after = after;
    }

    Gap::~Gap()
    {
    }

    std::ostream& operator<<(std::ostream& os, Gap &g)
    {
	  os << g.getDuration() << ": <" << g.getStart() << ", " << g.getEnd() << ">";
	  return os;
    }

    uint64_t Gap::getStart() const
    {
	return start;
    }

    uint64_t Gap::getEnd() const
    {
	return end;
    }

    uint64_t Gap::getDuration() const
    {
	return end - start;
    }

    Endpoint *Gap::getEndpointBefore() const
    {
	return before;
    }

    Endpoint *Gap::getEndpointAfter() const
    {
	return after;
    }

    void Gap::setEndpointBefore(Endpoint *before)
    {
	if(before != NULL)
	{
	    assert(before->getTime() < this->end);
	    this->start = before->getTime();
	}
	this->before = before;
    }

    void Gap::setEndpointAfter(Endpoint *after)
    {
	if(after != NULL)
	{
	    assert(after->getTime() > this->start);
	    this->end = after->getTime();
	}
	this->after = after;
    }
}
