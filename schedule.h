/* Schedule class, with functions for merging and finding gaps in schedules.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SCHEDULE_H
#define __SCHEDULE_H

#include <set>
#include <vector>
#include <boost/unordered_map.hpp>
#include <boost/cstdint.hpp>

#include "gap.h"
#include "endpoint.h"

namespace schedule
{
    // Data type for schedule intervals. Intervals represent "busy" periods in the schedule.
    typedef struct
    {
	Endpoint *start;
	Endpoint *end;
    } Interval;

    // Schedule class, with functions for merging and finding gaps in schedules.
    class Schedule
    {
	public:
	    // Comparator for gaps between schedule intervals.
	    struct gapCompare
	    {
		// Sort gaps by longest gap first, then earliest start time as tiebreaker.
		bool operator() (const Gap *a, const Gap *b) const
		{
		    if(a->getDuration() > b->getDuration())
			return true;
		    if((a->getDuration() == b->getDuration()) &&
			    (a->getStart() < b->getStart()))
			return true;

		    return false;
		}
	    };

	    // Set of gaps between busy intervals in the schedule. Gaps are sorted by
	    // duration (longest gap first), then by start time (earliest first).
	    typedef std::set<Gap *, gapCompare> GapSet;

	private:
	    // Node for the doubly linked list representing the schedule. Each node is an 
	    // interval endpoint, and may also have a gap preceding the endpoint (only for 
	    // start endpoints).
	    typedef struct node
	    {
		Endpoint *endpoint;
		struct node *next;
		struct node *prev;
		// Node destructor. Deletes both the endpoint and the gap for this node.
		~node()
		{
		    // Gaps are associated with endpoints but do not belong to them - we 
		    // create them and should delete them
		    Gap *gap = endpoint->getGap();
		    delete gap;
		    delete endpoint;
		}
	    } Node;

	    // Add an interval, start looking at the node specified
	    // Returns a pointer to the last node we reached or added
	    // Time of the node returned will be no later than the end of the interval 
	    // that was added
	    Node *addInterval(Interval interval, Node *begin);
	    // Delete the schedule list, and all the nodes in it.
	    void deleteSchedule();
	    // Delete all the gaps in the gap set.
	    void deleteGapSetContents();
	    // Deletes a node in the schedule list and removes the deleted endpoint from
	    // the gap preceding or following it, if there is one.
	    void deleteNode(Node *toDelete);

	    // Set of gaps between schedule intervals. Should only be modified using
	    // buildGapSet and insertIntoGap, otherwise it cannot be guaranteed to be
	    // valid.
	    GapSet gapSet;
	    // Pointers from gaps back to their locations in the schedule list
	    boost::unordered_map<Gap *, Node *> gapLocations;
	    // Doubly linked list representing the schedule.
	    Node *schedule;
	    // Pointer to the end of the schedule list.
	    Node *schedule_tail;

	public:
	    // Constructor. Creates an empty schedule. 
	    Schedule();
	    // Destructor. Deletes the schedule and its gap set. 
	    virtual ~Schedule();

	    // Merge another schedule into this schedule. Any overlapping or contiguous
	    // intervals are merged into single intervals. O(n), where n is the number of
	    // intervals in this schedule.
	    //
	    // This operation invalidates the gap set, so it should be rebuilt before using
	    // it again.
	    void addSchedule(const Schedule *other);

	    // Add an interval to the schedule, with the specified start and end times. O(n),
	    // where n is the number of intervals already in the schedule.
	    //	
	    // This operation invalidates the gap set, so it should be rebuilt before using
	    // it again.
	    void addInterval(uint64_t start, uint64_t end);

	    // Add an interval to the schedule. O(n), where n is the number of intervals
	    // already in the schedule.
	    //	
	    // This operation invalidates the gap set, so it should be rebuilt before using
	    // it again.
	    void addInterval(Interval interval);

	    // Add a vector of intervals to the schedule. The intervals should be in order,
	    // from earliest start time to latest, and should not overlap. O(n), where n is
	    // the number of intervals to add.
	    //	
	    // This operation invalidates the gap set, so it should be rebuilt before using
	    // it again.
	    void addIntervals(std::vector<Interval> *intervals);

	    // Builds the set of gaps between busy intervals in the schedule. If, after 
	    // building the gap set, the schedule is modified by any function other than 
	    // insertIntoGap, the gap set will no longer be valid and must be rebuilt by
	    // calling this function again. O(nlogn), where n is the number of intervals in
	    // the schedule.
	    GapSet &buildGapSet();

	    // Inserts an interval into the gap specified, and modifies both the schedule
	    // and gap set accordingly.
	    GapSet &insertIntoGap(Interval interval, Gap *gap);

	    // Inserts an interval into the gap specified, and modifies both the schedule
	    // and gap set accordingly.
	    GapSet &insertIntoGap(uint64_t start, uint64_t end, Gap *gap);

	    // Cleans up a schedule by removing all intervals that end before the given time.
	    // If an interval starts before the specified time, but ends after it, the 
	    // interval will not be removed in the clean up.
	    void cleanUp(uint64_t time);

	    // Returns the start time of the schedule.
	    uint64_t getStart() const;
	    // Returns the end time of the schedule.
	    uint64_t getEnd() const;
	    // Creates a string representation of the schedule.
	    std::string toString() const;
	    // Converts the schedule to a vector of non-overlapping intervals, in order of
	    // start time, earliest to latest.
	    std::vector<Interval> *toIntervalVector() const;
    };
}

#endif
