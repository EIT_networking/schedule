/* Tests for scheduling library.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <string>
#include <iterator>
#include <boost/cstdint.hpp>

#include "test.h"
#include "schedule.h"

using std::cout;
using std::endl;
using namespace schedule;

// Run all tests and print the results.
int main(void)
{
    test("Creating empty schedule", createEmptySchedule);
    test("Adding some intervals to an empty schedule", addIntervalsToEmptySchedule);
    test("Adding some intervals to an existing schedule", addIntervalsToExistingSchedule);
    test("Adding intervals before an existing schedule", addIntervalsBefore);
    test("Deleting a schedule", deleteSchedule);
    test("Cleaning up a schedule", cleanUpSchedule);
    test("Making gap set", makeGapSet);
    test("Inserting intervals into gaps", insertIntoGap);
    return 0;
}

void test(std::string str, bool (*test_fn)())
{
    cout << str << "........" << ((*test_fn)() ? "pass" : "fail") << endl;
}

Interval makeInterval(uint64_t start, uint64_t end)
{
    Interval interval = {new Endpoint(start, Endpoint::START), new Endpoint(end, Endpoint::END)};
    return interval;
}

bool createEmptySchedule()
{
    try
    {
	Schedule schedule;
    }
    catch(...)
    {
	return false;
    }
    return true;
}

std::vector<Interval> *makeIntervals(uint64_t start, unsigned int duration, unsigned int spacing, int count)
{
    std::vector<Interval> *intervals = new std::vector<Interval>;
    for(int i = 0; i < count; i++)
    {
	intervals->push_back(makeInterval(spacing*i + start, spacing*i + start + duration));
    }
    return intervals;
}

std::string makeScheduleString(uint64_t start, unsigned int duration, unsigned int spacing, int count, bool newline)
{
    std::stringstream scheduleString;
    for(int i = 0; i < count; i++)
    {
	scheduleString << "<" << spacing*i + start << " (" << (i == 0 ? start : spacing - duration) << "), " << 
		    spacing*i + start + duration << ">";
	if(i < count - 1)
	    scheduleString << ", ";
    }
    if(newline)
	scheduleString << endl;

    return scheduleString.str();
}

bool addIntervalsToEmptySchedule()
{
    try
    {
	Schedule schedule;
	std::string expected;

	std::vector<Interval> *intervals = makeIntervals(1, 3, 5, 10);
	expected = makeScheduleString(1, 3, 5, 10);
	schedule.addIntervals(intervals);
	delete intervals;

	std::string str = schedule.toString();

	if(str == expected)
	    return true;
	else
	    return false;
    }
    catch(...)
    {
	return false;
    }
}

bool addIntervalsToExistingSchedule()
{
    try
    {
	Schedule schedule;

	std::vector<Interval> *intervals = makeIntervals(1, 3, 5, 10);
	schedule.addIntervals(intervals);
	delete intervals;

	intervals = makeIntervals(0, 2, 3, 15);
	schedule.addIntervals(intervals);
	delete intervals;

	std::string expected = "<0 (0), 5>, <6 (1), 14>, <15 (1), 20>, <21 (1), 29>, <30 (1), 35>, <36 (1), 44>, <46 (2), 49>\n";
	std::string str = schedule.toString();
	if(str == expected)
	    return true;
	else
	    return false;
    }
    catch(...)
    {
	return false;
    }
}

bool addIntervalsBefore()
{
    try
    {
	Schedule schedule;
	std::stringstream expected;

	std::vector<Interval> *intervals = makeIntervals(100, 3, 5, 10);
	std::string partial = makeScheduleString(100, 3, 5, 10);
	// Change gap size in expected string
	// i.e. change from <100 (100)... to <100 (68)... since we add some intervals to the start
	partial.replace(6, 3, "68");
	schedule.addIntervals(intervals);
	delete intervals;

	intervals = makeIntervals(1, 3, 7, 5);
	expected << makeScheduleString(1, 3, 7, 5, false) << ", " << partial;
	schedule.addIntervals(intervals);
	delete intervals;

	std::string str = schedule.toString();

	if(str == expected.str())
	    return true;
	else
	    return false;
    }
    catch(...)
    {
	return false;
    }
}

// Run under valgrind or similar to test for memory leaks
bool deleteSchedule()
{
    try
    {
	Schedule *schedule = new Schedule();
	delete schedule;
	schedule = new Schedule();

	std::vector<Interval> *intervals = makeIntervals(1, 3, 5, 10);
	schedule->addIntervals(intervals);
	delete intervals;

	intervals = makeIntervals(0, 2, 3, 15);
	schedule->addIntervals(intervals);
	delete intervals;

	delete schedule;
    }
    catch(...)
    {
	return false;
    }

    return true;
}

bool cleanUpSchedule()
{
    try
    {
	Schedule schedule;

	std::vector<Interval> *intervals = makeIntervals(1, 3, 5, 10);
	std::string expected = makeScheduleString(1, 3, 5, 10);
	schedule.addIntervals(intervals);
	delete intervals;

	schedule.cleanUp(0);

	std::string str = schedule.toString();

	if(str != expected)
	    return false;

	schedule.cleanUp(10);

	str = schedule.toString();
	expected = "<11 (2), 14>, <16 (2), 19>, <21 (2), 24>, <26 (2), 29>, <31 (2), 34>, <36 (2), 39>, <41 (2), 44>, <46 (2), 49>\n";

	if(str != expected)
	    return false;

	schedule.cleanUp(5);

	str = schedule.toString();
	expected = "<11 (2), 14>, <16 (2), 19>, <21 (2), 24>, <26 (2), 29>, <31 (2), 34>, <36 (2), 39>, <41 (2), 44>, <46 (2), 49>\n";

	if(str != expected)
	    return false;

	schedule.cleanUp(17);

	str = schedule.toString();
	expected = "<16 (2), 19>, <21 (2), 24>, <26 (2), 29>, <31 (2), 34>, <36 (2), 39>, <41 (2), 44>, <46 (2), 49>\n";

	if(str != expected)
	    return false;

	schedule.cleanUp(21);

	str = schedule.toString();
	expected = "<21 (2), 24>, <26 (2), 29>, <31 (2), 34>, <36 (2), 39>, <41 (2), 44>, <46 (2), 49>\n";

	if(str != expected)
	    return false;

	schedule.cleanUp(55);

	str = schedule.toString();
	expected = "\n";

	if(str != expected)
	    return false;

	return true;
    }
    catch(...)
    {
	return false;
    }
}

bool makeGapSet()
{
    try
    {
    	Schedule schedule;

	std::vector<Interval> *intervals = makeIntervals(5, 3, 13, 10);
	schedule.addIntervals(intervals);
	delete intervals;

	intervals = makeIntervals(0, 2, 7, 15);
	schedule.addIntervals(intervals);
	delete intervals;

	Schedule::GapSet gs = schedule.buildGapSet();
	std::stringstream gaps;
	for(Schedule::GapSet::iterator it = gs.begin(); it != gs.end(); it++)
	    gaps << **it << ", ";
	gaps << endl;

	std::string expected = "10: <112, 122>, 9: <100, 109>, 5: <9, 14>, 5: <23, 28>, 5: <37, 42>, 5: <51, 56>, 5: <65, 70>, 5: <86, 91>, 4: <73, 77>, 4: <79, 83>, 3: <2, 5>, 3: <60, 63>, 3: <93, 96>, 2: <16, 18>, 2: <47, 49>, 1: <30, 31>, 1: <34, 35>, 0: <0, 0>, \n";
	if(gaps.str() == expected)
	    return true;
	else
	    return false;
    }
    catch(...)
    {
	return false;
    }
}

bool insertIntoGap()
{    
    try
    {
    	Schedule schedule;

	std::vector<Interval> *intervals = makeIntervals(5, 3, 13, 10);
	schedule.addIntervals(intervals);
	delete intervals;

	intervals = makeIntervals(0, 2, 7, 15);
	schedule.addIntervals(intervals);
	delete intervals;

	Schedule::GapSet gs = schedule.buildGapSet();

	for(Schedule::GapSet::iterator it = gs.begin(); it != gs.end(); it++)
	{
	    Gap *gap = *it;
	    if(gap->getDuration() > 2)
		schedule.insertIntoGap(gap->getStart() + 1, gap->getEnd() - 1, gap);
	}

	std::string str = schedule.toString();
	std::string expected = "<0 (0), 2>, <3 (1), 4>, <5 (1), 9>, <10 (1), 13>, <14 (1), 16>, <18 (2), 23>, <24 (1), 27>, <28 (1), 30>, <31 (1), 34>, <35 (1), 37>, <38 (1), 41>, <42 (1), 47>, <49 (2), 51>, <52 (1), 55>, <56 (1), 60>, <61 (1), 62>, <63 (1), 65>, <66 (1), 69>, <70 (1), 73>, <74 (1), 76>, <77 (1), 79>, <80 (1), 82>, <83 (1), 86>, <87 (1), 90>, <91 (1), 93>, <94 (1), 95>, <96 (1), 100>, <101 (1), 108>, <109 (1), 112>, <113 (1), 121>, <122 (1), 125>\n";

	if(str == expected)
	    return true;
	else
	    return false;
    }
    catch(...)
    {
	return false;
    }
}
