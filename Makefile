# Makefile
#
# © Copyright 2016 Emma Fitzgerald 
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SOURCE=schedule.cc schedule.h endpoint.cc endpoint.h gap.cc gap.h
TEST_SOURCE=test.cc test.h
LIBOBJ=schedule.o endpoint.o gap.o
TESTOBJ=test.o
BIN=schedule
LIBRARY=libschedule.a
SHARED_LIB=libschedule.so
CXX=g++
CPPFLAGS=-Wall -Wextra -pedantic
GCH=endpoint.h.gch gap.h.gch schedule.h.gch test.h.gch

#------------------------------------------------------------------------------

all: $(BIN)

test: $(BIN)
	./schedule

$(BIN): $(TESTOBJ) $(LIBOBJ)
	$(CXX) $(LIBOBJ) $(TESTOBJ) -o $(BIN)

library: $(LIBOBJ)
	ar crf $(LIBRARY) $(LIBOBJ)

shared_library: $(LIBOBJ)
	$(CXX) $(LIBOBJ) -shared -o $(SHARED_LIB)
	
$(TESTOBJ): $(LIBOBJ) $(TEST_SOURCE)
	$(CXX) $(CPPFLAGS) -I$(INCLUDES) -l$(LIBS) -c $(TEST_SOURCE)

$(LIBOBJ): $(SOURCE)
	$(CXX) $(CPPFLAGS) -fPIC -I$(INCLUDES) -l$(LIBS) -c $(SOURCE)

clean:
	rm -f $(BIN) $(LIBOBJ) $(TESTOBJ) $(GCH) $(LIBRARY) $(SHARED_LIB)
