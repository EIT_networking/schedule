/* Endpoint class for endpoints of schedule intervals.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/cstdint.hpp>
#include <assert.h>
#include "endpoint.h"
#include "gap.h"

namespace schedule
{
    Endpoint::Endpoint(uint64_t time, endpointType type, Gap *gap)
    {
	this->time = time;
	this->type = type;
	this->gap = gap;
    }

    Endpoint::~Endpoint()
    {
    }

    uint64_t Endpoint::getTime() const
    {
	return this->time;
    }

    Endpoint::endpointType Endpoint::getType() const
    {
	return this->type;
    }

    Gap *Endpoint::getGap() const
    {
	return this->gap;
    }

    // gap may be NULL
    void Endpoint::setGap(Gap *gap)
    {
	this->gap = gap;
    }
}
