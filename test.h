/* Tests for scheduling library.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TEST_H
#define __TEST_H

#include <string>
#include <boost/cstdint.hpp>

#include "schedule.h"

// Run the test pointed to by test_fn and output the results.
void test(std::string str, bool (*test_fn)());
// Create an interval with the given start and end times.
schedule::Interval makeInterval(uint64_t start, uint64_t end);
// Create a schedule, starting at the given start time, with count intervals of the given duration, 
// and gaps of duration spacing in between each pair of intervals.
std::vector<schedule::Interval> makeSchedule(uint64_t start, unsigned int duration, unsigned int spacing, int count);
// Create a string representation of a schedule, starting at the given start time, with count intervals of the given duration, 
// and gaps of duration spacing in between each pair of intervals.
std::string makeScheduleString(uint64_t start, unsigned int duration, unsigned int spacing, int count, bool newline = true);

// Tests
bool createEmptySchedule();
bool addIntervalsToEmptySchedule();
bool addIntervalsToExistingSchedule();
bool addIntervalsBefore();
bool deleteSchedule();
bool cleanUpSchedule();
bool makeGapSet();
bool insertIntoGap();

#endif
