/* Endpoint class for endpoints of schedule intervals.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ENDPOINT_H
#define __ENDPOINT_H

#include <boost/cstdint.hpp>
#include "gap.h"

namespace schedule
{
    class Gap;
    
    // An endpoint of a schedule interval. Endpoints can be either START endpoints
    // (the beginning of an interval) or END endpoints (the end of an interval).
    // An endpoint consists of a type (START or END) and a time.
    class Endpoint
    {
	public:
	    enum endpointType {START, END};

	private:
	    Gap *gap;
	    uint64_t time;
	    endpointType type;

	public:
	    // Constructor. Time and type must be specified, and optionally a gap 
	    // (default NULL) to precede this endpoint, if type is Endpoint::START, or 
	    // to follow it, if type is Endpoint::END.
	    Endpoint(uint64_t time, endpointType type, Gap *gap = NULL);

	    // Destructor. If there is a gap associated with this endpoint, it is not 
	    // deleted. 
	    virtual ~Endpoint();

	    // Returns the time associated with this endpoint.
	    uint64_t getTime() const;
	    // Returns the type of this endpoint, either Endpoint::START or Endpoint::END.
	    endpointType getType() const;
	    // Returns the gap following this endpoint, or NULL if there is none.
	    Gap *getGap() const;
	    // Sets the gap preceding this endpoint (for START endpoints) or following it
	    // (for END endpoints).
	    void setGap(Gap *gap);
    };
}

#endif
