/* Schedule class, with functions for merging and finding gaps in schedules.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstddef>
#include <boost/cstdint.hpp>
#include <boost/integer_traits.hpp>
#include <sstream>

#include "schedule.h"
#include "gap.h"
#include "endpoint.h"

namespace schedule
{
    Schedule::Schedule()
    {
	schedule = NULL;
	schedule_tail = NULL;
    }

    Schedule::~Schedule()
    {
	deleteGapSetContents();
	deleteSchedule();
    }

    void Schedule::deleteSchedule()
    {
	Node *current;
	Node *next;
	for(current = schedule; current != NULL; current = next)
	{
	    next = current->next;

	    delete current;
	}
	schedule = NULL;
	schedule_tail = NULL;
    }

    void Schedule::deleteNode(Node *toDelete)
    {
	if(toDelete == schedule)
	    schedule = toDelete->next;
	if(toDelete == schedule_tail)
	    schedule_tail = toDelete->prev;
	Node *before = toDelete->prev;
	Node *after = toDelete->next;
	if(before)
	    before->next = after;
	if(after)
	    after->prev = before;

	// Make gap endpoints invalid if any 
	if(toDelete->endpoint->getType() == Endpoint::START  && toDelete->endpoint->getGap() != NULL)
	    toDelete->endpoint->getGap()->setEndpointAfter(NULL);
	if(toDelete->endpoint->getType() == Endpoint::END && toDelete->next != NULL && toDelete->next->endpoint->getGap() != NULL)
	    toDelete->next->endpoint->getGap()->setEndpointBefore(NULL);

	delete toDelete;
    }

    void Schedule::addInterval(uint64_t start, uint64_t end)
    {
	Endpoint *start_ep = new Endpoint(start, Endpoint::START);
	Endpoint *end_ep = new Endpoint(end, Endpoint::END);
	Interval interval = {start_ep, end_ep};
	addInterval(interval);
    }

    // O(n). If you have multiple intervals to add, use addIntervals() instead
    void Schedule::addInterval(Interval interval)
    {
	addInterval(interval, schedule);
    }

    std::vector<Interval> *Schedule::toIntervalVector() const
    {
	std::vector<Interval> *intervals = new std::vector<Interval>;
	Node *curr = NULL;
	Interval interval;

	for(curr = schedule; curr; curr = curr->next)
	{
	    if(curr->endpoint->getType() == Endpoint::START)
		interval.start = new Endpoint(uint64_t(curr->endpoint->getTime()), Endpoint::START);
	    else
	    {
		// should be an END endpoint
		interval.end = new Endpoint(uint64_t(curr->endpoint->getTime()), Endpoint::END);
		intervals->push_back(interval);
	    }
	}

	return intervals;
    }

    void Schedule::addSchedule(const Schedule * other)
    {
	std::vector<Interval> *intervals = other->toIntervalVector();
	addIntervals(intervals);
	delete intervals;
    }

    // Add a vector of intervals to the schedule. Intervals to add must be non-overlapping and in order of start time
    void Schedule::addIntervals(std::vector<Interval> *intervals)
    {
	Node *last = schedule;
	std::vector<Interval>::iterator it;

	// Walk through the vector, adding the intervals in order
	for(it = intervals->begin(); it != intervals->end(); ++it)
	{
	    last = addInterval(*it, last);
	}
    }

    // Add an interval, start looking at the node specified
    // Returns a pointer to the last node we reached or added
    // Time of the node returned will be no later than the end of the interval that was added
    Schedule::Node *Schedule::addInterval(Interval interval, Node *begin)
    {
	assert(interval.start != NULL);
	assert(interval.end != NULL);
	// Create new nodes to represent the interval to be added
	Node *startNode = new Node();
	startNode->endpoint = interval.start;
	Node *endNode = new Node();
	endNode->endpoint = interval.end;

	Node *current = begin;

	if(schedule == NULL)
	{
	    // Should not give us a valid node pointer if there is no list!
	    assert(current == NULL);
	    // list is empty
	    startNode->prev = NULL;
	    // This is the first interval in the schedule, so we have an unbounded gap before it, limited onnly by the time origin
	    // i.e. starts at time = 0
	    Gap *gap = new Gap(0, startNode->endpoint->getTime(), NULL, startNode->endpoint);
	    startNode->endpoint->setGap(gap);
	    schedule = startNode;

	    endNode->prev = startNode;
	    endNode->next = NULL;
	    startNode->next = endNode;
	    schedule_tail = endNode;
	    return endNode;
	}

	// We should not receive NULL unless the list is empty
	assert(current != NULL);

	// Find the correct place in the list for the start node
	Node *before = NULL;
	for(; current != NULL; current = current->next)
	{
	    if(current->endpoint->getTime() > interval.start->getTime())
	    {
		// We have gone past where the start endpoint should be inserted
		break;
	    }
	    before = current;
	}

	// Insert start node

	if(before == NULL)
	{
	    // Case 1: we are earlier than any existing node, i.e. new head node
	    startNode->next = schedule;
	    startNode->prev = NULL;
	    Gap *gap = new Gap(0, startNode->endpoint->getTime(), NULL, startNode->endpoint);
	    startNode->endpoint->setGap(gap);
	    schedule = startNode;
	    before = startNode;
	}
	else if(current == NULL)
	{
	    // Case 2: start time of the new interval is later than all existing nodes, new tail node
	    startNode->next = NULL;
	    if(before->endpoint->getTime() == startNode->endpoint->getTime())
	    {
		// Previous interval exactly joins up with new interval, delete end node and
		// do not insert start node
		Node *temp = before->prev;
		deleteNode(before);
		before = temp;
		schedule_tail = before;
	    }
	    else
	    {
		before->next = startNode;
		startNode->prev = before;
		schedule_tail = startNode;

		Gap *gap = new Gap(before->endpoint->getTime(), startNode->endpoint->getTime(), before->endpoint, startNode->endpoint);
		startNode->endpoint->setGap(gap);
		before = startNode;
	    }
	}
	else
	{
	    if(before->endpoint->getType() == Endpoint::END)
	    {
		if(before->endpoint->getTime() >= startNode->endpoint->getTime())
		{
		    // Previous interval exactly joins up with start of new interval, delete end node &
		    // do not insert start node
		    Node *temp = before->prev;
		    deleteNode(before);
		    before = temp;
		}
		else
		{
		    // We are starting after the end of another interval, there will be a gap in between
		    startNode->next = before->next;
		    startNode->next->prev = startNode;
		    startNode->prev = before;
		    before->next = startNode;

		    Gap *gap = new Gap(before->endpoint->getTime(), startNode->endpoint->getTime(), before->endpoint, startNode->endpoint);
		    startNode->endpoint->setGap(gap);
		    before = startNode;
		}
	    }
	    else
	    {
		assert(before->endpoint->getType() == Endpoint::START);

		// Do nothing. Existing interval starts before or at the same time as us, we do not need a new start node
	    }
	}
	
	// Delete the startNode if it was not used
	if(before != startNode)
	    delete startNode;

	Node *temp = NULL;

	// Find the correct place for the end node and insert it, whilst deleting any intervening nodes
	for(current = before->next; current != NULL; current = temp)
	{
	    if(current->endpoint->getTime() >= endNode->endpoint->getTime())
	    {
		// found the right spot, fix the list and exit the loop
		before->next = current;
		current->prev = before;
		break;
	    }
	    else
	    {
		// save the next pointer so we can keep iterating
		temp = current->next;
		deleteNode(current);
	    }
	}

	// No need to check if we are the first node - there will always be an interval start node before us since we are an end node
	
	if(current == NULL)
	{
	    // We are after all existing nodes, i.e. end of list
	    endNode->next = NULL;
	    endNode->prev = before;
	    before->next = endNode;
	    schedule_tail = endNode;
	    return endNode;
	}
	else
	{
	    // in the middle of the list somewhere
	    
	    if(current->endpoint->getTime() == endNode->endpoint->getTime())
	    {
		if(current->endpoint->getType() == Endpoint::END)
		{
		    // There is already an end of an interval at this time, nothing to do and endNode is not needed.
		    delete endNode;
		    return current;
		}
		else
		{
		    assert(current->endpoint->getType() == Endpoint::START);

		    // Another interval starts right when we should end, delete start node, don't insert end node
		    Node *temp = current->prev;
		    deleteNode(current);
		    delete endNode;
		    return temp;
		}
	    }

	    if(current->endpoint->getType() == Endpoint::END)
	    {
		// There is an interval end straight after us, i.e. our new interval would end in the middle of an existing one
		// so don't insert endNode
		delete endNode;
		return before;
	    }

	    assert(current->endpoint->getType() == Endpoint::START);

	    endNode->next = current;
	    endNode->prev = before;
	    endNode->next->prev = endNode;
	    before->next = endNode;

	    // There will be a gap after us since we are not the last node
	    Node *nextStart = endNode->next;
	    // Remove old gap
	    Gap *gap = nextStart->endpoint->getGap();
	    delete gap;
	    // Create new gap with updated data
	    gap = new Gap(endNode->endpoint->getTime(), nextStart->endpoint->getTime(), endNode->endpoint, nextStart->endpoint);
	    nextStart->endpoint->setGap(gap);
	    return endNode;
	}

	// Shouldn't happen
	return NULL;
    }

    void Schedule::cleanUp(uint64_t time)
    {
	Node *current = schedule;
	Node *temp = NULL;
	for(; current != NULL; current = temp)
	{
	    temp = current->next;
	    if(current->endpoint->getTime() >= time)
		break;
	    // Don't delete half an interval, even if it starts before the clean-up time
	    if(current->endpoint->getType() == Endpoint::END)
	    {
		deleteNode(current->prev);
		deleteNode(current);
	    }
	}
    }

    void Schedule::deleteGapSetContents()
    {	
	for(GapSet::iterator it = gapSet.begin(); it != gapSet.end(); it++)
	    delete *it;
    }

    // Builds and returns gap set. Since many operations may alter the gap set without updating it, we
    // rebuild it each time. (It would be to slow to always update it.) Call this function sparingly.
    // O(nlogn)
    Schedule::GapSet &Schedule::buildGapSet()
    {
	// Clear the old contents of the gap set
	deleteGapSetContents();
	gapSet.clear();

	// Clear the old contents of the gap locations map
	gapLocations.clear();

	Node *current = schedule;
	for(; current != NULL; current = current->next)
	{
	    if(current->endpoint->getType() != Endpoint::START)
		continue;
	    Gap *gap = current->endpoint->getGap();
	    if(gap != NULL && gap->getDuration() > uint64_t(0.0))
	    {
		Gap *copy = new Gap(gap->getStart(), gap->getEnd(), gap->getEndpointBefore(), gap->getEndpointAfter());
		gapSet.insert(copy);
		gapLocations[copy] = current;
	    }
	}

	// Add zero-duration gap as an achor element at the end to
	// assist with finding gaps for packets
	Gap *anchor = new Gap(uint64_t(0), uint64_t(0));
	gapSet.insert(anchor);

	return gapSet;
    }

    Schedule::GapSet &Schedule::insertIntoGap(uint64_t start, uint64_t end, Gap *gap)
    {
	Endpoint *start_ep = new Endpoint(start, Endpoint::START);
	Endpoint *end_ep = new Endpoint(end, Endpoint::END);
	Interval interval = {start_ep, end_ep};
	return insertIntoGap(interval, gap);
    }

    Schedule::GapSet &Schedule::insertIntoGap(Interval interval, Gap *gap)
    {
	assert(gap != NULL);
	uint64_t start = interval.start->getTime();
	uint64_t end = interval.end->getTime();

	// Check that interval fits in gap
	assert(end - start <= gap->getDuration());
	
	// Insert new interval into schedule
	Node *end_node = addInterval(interval, gapLocations[gap]->prev);
	
	// Remove old gap and insert both new gaps into gap set
	gapSet.erase(gap);
	// Gap before is attached to start node of the new interval
	Gap *gap_before = NULL;
	if(end_node->prev)
	    gap_before = end_node->prev->endpoint->getGap();
	// Gap after is attached to start node of the next interval after the newly inserted one
	Gap *gap_after = NULL;
	if(end_node->next)
	    gap_after = end_node->next->endpoint->getGap();
	Gap *gap_before_copy = NULL;
	Gap *gap_after_copy = NULL;
	if(gap_before)
	{
	    gap_before_copy = new Gap(gap_before->getStart(), gap_before->getEnd(), gap_before->getEndpointBefore(),
					gap_before->getEndpointAfter());
	    gapSet.insert(gap_before_copy);
	}
	if(gap_after)
	{
	    gap_after_copy = new Gap(gap_after->getStart(), gap_after->getEnd(), gap_after->getEndpointBefore(),
					gap_after->getEndpointAfter());
	    gapSet.insert(gap_after_copy);
	}
	
	// Remove old gap from map and insert both new gaps into map
	gapLocations.erase(gap);
	if(gap_before)
	    gapLocations[gap_before_copy] = end_node->prev;
	if(gap_after)
	    gapLocations[gap_after_copy] = end_node->next;

	// Delete our copy of old gap
	delete gap;

	return gapSet;
    }

    // Returns the start of the first interval in the schedule
    uint64_t Schedule::getStart() const
    {
	if(schedule == NULL)
	    return uint64_t(0);
	return schedule->endpoint->getTime();
    }

    uint64_t Schedule::getEnd() const
    {
	if(schedule_tail == NULL)
	    return uint64_t(0);
	return schedule_tail->endpoint->getTime();
    }

    std::string Schedule::toString() const
    {
	std::stringstream output;
	for(Node *curr = schedule; curr != NULL; curr=curr->next)
	{
	    if(curr->endpoint->getType() == Endpoint::START)
	    {
		output << "<" << curr->endpoint->getTime();
		if(curr->endpoint->getGap())
		    output << " (" << curr->endpoint->getGap()->getDuration() << ")";
		output << ", ";
	    }
	    else
	    {
		output << curr->endpoint->getTime() << ">";
		if(curr->next != NULL)
		    output << ", ";
	    }
	}
	output << std::endl;
	return output.str();
    }
}
