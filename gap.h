/* Gap class for gaps: empty time between two scheduled intervals.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GAP_H
#define __GAP_H

#include <cstddef>
#include <iostream>
#include <boost/cstdint.hpp>

#include "endpoint.h"

namespace schedule
{
    class Endpoint;

    // Gap class for durations between scheduled intervals. Gaps have start and end times,
    // and optionally interval endpoints that occur directly before or after the gap.
    class Gap
    {
	private:
	   uint64_t start;
	   uint64_t end;
	   Endpoint *before;
	   Endpoint *after;

	public:
	    // Constructor. Start and end times must be specified, and optionally pointers
	    // to endpoints directly before and/or after this gap can be provided.
	    // Endpoint times should match the provided start and/or end times for the gap.
	    Gap(uint64_t start, uint64_t end, Endpoint *before = NULL, Endpoint *after = NULL);
	    // Destrcutor. Does not delete endpoints before or after this gap.
	    virtual ~Gap();

	    // Returns the time when this gap begins.
	    uint64_t getStart() const;
	    // Returns the time when this gap ends.
	    uint64_t getEnd() const;
	    // Returns the duration of this gap (end time minus start time).
	    uint64_t getDuration() const;
	    // Sets the endpoint occurring directly before this gap, and sets the gap start
	    // time to match the endpoint time. The time of the endpoint must be before the
	    // end time of this gap.
	    void setEndpointBefore(Endpoint *before);
	    // Sets the endpoint occurring directly after this gap, and sets the gap end time
	    // to match the endpoint time. The time of the endpoint must be after the start
	    // time of this gap.
	    void setEndpointAfter(Endpoint *after);
	    // Returns a pointer to the endpoint occurring directly before this gap.
	    Endpoint *getEndpointBefore() const;
	    // Returns a pointer to the endpoint ocurring directly after this gap.
	    Endpoint *getEndpointAfter() const;
    };

    // Creates a string representation of this gap in the format 
    // "duration: <start time, end time>"
    std::ostream& operator<<(std::ostream& os, Gap &g);
}

#endif
